import keras

""" Example of callbacks implementation. Very usefull tool in Keras """

callbacks_list = [

    keras.callbacks.EarlyStopping(      # checking results and stopping training after acc decent
        monitor='acc',
        patience=1,
    ),

    keras.callbacks.ModelCheckpoint(    # saving the best model
        filepath='model.h5',
        monitor='val_loss',
        save_best_only=True,
    ),

    keras.callbacks.ReduceLROnPlateau(      # improving learning rate to get out of the local minimum
        monitor='val_loss',
        factor=0.1,
        patience=10,
    ),

    keras.callbacks.TensorBoard(
        log_dir='logs',
        histogram_freq=1,
        embeddings_freq=1,
    )
]

model.compile(optimizer='rmsprop',
              loss='binary_crossentropy',
              metrics=['acc'])

model.fit(x, y,
          epochs=10,
          batch_size=32,
          callbacks=callbacks_list,
          validation_data=(x_val, y_val))