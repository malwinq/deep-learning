import numpy as np

num_validation = 10000

np.random.shuffle(data)

# validation data
validation_data = data[:num_validation_samples]
data = data[num_validation_samples:]

# training data
training_data = data[:]

# training the model
model = get_model()
model.train(training_data)
validation_score = model.evaluate(validation_data)

# ...
# improving the model
# ...

model = get_model()
model.train(np.concatenate([training_data, validation_data]))
test_score = model.evaluate(test_data)