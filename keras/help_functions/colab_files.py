from zipfile import ZipFile

file_name = "cats_and_dogs_small.zip"

with ZipFile(file_name, 'r') as zip:
	zip.extractall()
	print('Done')