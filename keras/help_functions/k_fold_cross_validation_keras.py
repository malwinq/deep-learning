import numpy as np

""" The implementation of k-fold cross validation algorithm """

k = 4
num_val_samples = len(data) // k

np.random.shuffle(data)

validation_scores = []
for fold in range(k):
	print('processing fold#', i)
	validation_data = data[fold * num_val_samples: (fold+1) * num_val_samples]
	training_data = data[:num_val_samples * fold] + data[num_val_samples * (fold + 1):]
	
	model = get_model()
	model.train(training_data)
	
	validation_score = np.average(validation_scores)
	validation_scores.append(validation_score)
	
validation_score = np.average(validation_scores)

model = get_model()
model.train(data)
test_score = model.evaluate(test_data)