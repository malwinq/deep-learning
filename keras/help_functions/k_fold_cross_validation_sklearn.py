from sklearn.model_selection import StratifiedKFold
from sklearn.base import clone

skfolds = StratifiedKFold(n_splits=3, random_state=42)

for train_ind, test_ind in skfolds.split(x_train, y_train):
    clone = clone(sgd)
    x_train_folds = x_train[train_ind]
    y_train_folds = (y_train[train_ind])
    x_test_folds = x_test[train_ind]
    y_test_folds = (y_test[train_ind])

    clone.fit(x_train_folds, y_train_folds)
    y_pred = clone.predict(x_test_folds)
    n_correct = sum(y_pred == y_test_folds)

    print(n_correct / len(y_pred))
