import keras
import numpy as np
from keras import layers
import random
import sys
from keras.models import model_from_json

""" Generating poems based on Adam Mickiewicz's work """

# preparing data
path = r'/home/malwina/repo/deep-learning/AdamMickiewicz.txt'

text = open(path).read().lower()

maxlen = 60         # length of sentence
step = 3            # sampling period
sentences = []      # input of nn
next_chars = []     # output of nn
length = 300

for i in range(0, len(text) - maxlen, step):
    sentences.append(text[i:i+maxlen])
    next_chars.append(text[i+maxlen])
print('The number of sequences: ', len(sentences))

# unique chars
chars = sorted(list(set(text)))
print('The number of unique chars: ', len(chars))
char_indices = dict((char, chars.index(char)) for char in chars)

# creating vectors
x = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), len(chars)), dtype=np.bool)

for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        x[i, t, char_indices[char]] = 1
    y[i, char_indices[next_chars[i]]] = 1
# --------------------------------------------------------------------
"""     NEW MODEL
# network model
model = keras.models.Sequential()
model.add(layers.LSTM(128, input_shape=(maxlen, len(chars))))
model.add(layers.Dense(len(chars), activation='softmax'))

optimizer = keras.optimizers.RMSprop(lr=0.01)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)

# the loop of epochs
model.fit(x, y, batch_size=256, epochs=10)

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights("model.h5")
print("Saved model to disk")
"""
# LOAD OLD MODEL
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
model.load_weights("model.h5")
print("Loaded model from disk")
# --------------------------------------------------------------------

# prediction of next chars
def sample(preds, temperature=1.0):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

generated_text = 'litwo! ojczyzno moja, ty jesteś jak zdrowie. ' \
                 'ile cię trzeba '
maxlen = len(generated_text)
gen_text2 = 'litwo! ojczyzno moja, ty jesteś jak zdrowie. ' \
                 'ile cię trzeba '
# generated_text = text[start_index: start_index + maxlen]
# start_index = random.randint(0, len(text) - maxlen - 1)
print('Generating text based on: ' + generated_text)

# loop of softmax temperatures
for temperature in [0.39, 0.5, 0.6, 0.7, 1.0]:
    print('Temperature: ', temperature)
    print()
    sys.stdout.write(gen_text2)

    for i in range(length):
        sampled = np.zeros((1, maxlen, len(chars)))
        for t, char in enumerate(generated_text):
            sampled[0, t, char_indices[char]] = 1

        preds = model.predict(sampled, verbose=0)[0]
        next_index = sample(preds, temperature)
        next_char = chars[next_index]

        generated_text += next_char
        generated_text = generated_text[1:]

        sys.stdout.write(next_char)

        sys.stdout.flush()
    print('\n\n')