from keras.applications import inception_v3
from keras import backend as K
from keras.preprocessing import image
import numpy as np
import scipy
import imageio

""" Generating images using DeepDream framework, code based on 
    code fragments from Deep Learning book by Francois Chollet """


def eval_loss_and_grads(x):
    outs = fetch_loss_and_grads([x])
    loss_value = outs[0]
    grad_values = outs[1]
    return loss_value, grad_values


def gradient_ascent(x, iterations, step, max_loss=None):
    for i in range(iterations):
        loss_value, grad_values = eval_loss_and_grads(x)
        if max_loss is not None and loss_value > max_loss:
            break
        print('... Loss value', i, ':', loss_value)
        x += step * grad_values
    return x


def resize_img(img, size):
    img = np.copy(img)
    factors = (1,
               float(size[0]) / img.shape[1],
               float(size[1]) / img.shape[2],
               1)
    return scipy.ndimage.zoom(img, factors, order=1)


def save_img(img, fname):
    pil_img = deprocess_image(np.copy(img))
    imageio.imwrite(fname, pil_img)


def preprocess_image(image_path):
    img = image.load_img(image_path)
    img = image.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = inception_v3.preprocess_input(img)
    return img


def deprocess_image(x):
    if K.image_data_format() == 'channels_first':
        x = x.reshape((3, x.shape[2], x.shape[3]))
        x = x.transpose((1, 2, 0))
    else:
        x = x.reshape((x.shape[1], x.shape[2], 3))

    x /= 2.
    x += 0.5
    x *= 255.
    x = np.clip(x, 0, 255).astype('uint8')
    return x


# params
step = 0.01
num_octave = 3
octave_scale = 1.4
iterations = 20
max_loss = 10
directory = r'/content/drive/My Drive/colab_files/'
base_image_path = directory + 'my.jpg'

K.set_learning_phase(0)         # turn off the train phase options (using pre-trained model)

model = inception_v3.InceptionV3(weights='imagenet', include_top=False)

# importance of every layer (lower for shapes, higher for classes like birds etc.)
layer_contribution = {
    'mixed2': 0.2,
    'mixed3': 3.,
    'mixed4': 2.,
    'mixed5': 1.5,
}

layer_dict = dict([layer.name, layer] for layer in model.layers)

loss = K.variable(0.)

for layer_name in layer_contribution:
    coeff = layer_contribution[layer_name]
    activation = layer_dict[layer_name].output

    scaling = K.prod(K.cast(K.shape(activation), 'float32'))
    loss += coeff * K.sum(K.square(activation[:, 2: -2, 2: -2, :])) / scaling

dream = model.input
grads = K.gradients(loss, dream)[0]
grads /= K.maximum(K.mean(K.abs(grads)), 1e-7)      # normalization of gradients

outputs = [loss, grads]
fetch_loss_and_grads = K.function([dream], outputs)

img = preprocess_image(base_image_path)
original_shape = img.shape[1:3]
successive_shapes = [original_shape]

for i in range(1, num_octave):
    shape = tuple([int(dim / (octave_scale ** 1)) for dim in original_shape])
    successive_shapes.append(shape)

successive_shapes = successive_shapes[::-1]

original_img = np.copy(img)
shrunk_org_img = resize_img(img, successive_shapes[0])
l = 1

for shape in successive_shapes:
    print('Image size changing', shape)
    img = resize_img(img, shape)
    img = gradient_ascent(img,
                          iterations=iterations,
                          step=step,
                          max_loss=max_loss)

    upscaled_skrunk_org_img = resize_img(shrunk_org_img, shape)
    same_size_original = resize_img(original_img, shape)
    loss_detail = same_size_original - upscaled_skrunk_org_img

    img += loss_detail
    shrunk_org_img = resize_img(original_img, shape)
    save_img(img, fname=directory + 'dream_at_scale_' + str(l) + '.png')
    l += 1

save_img(img, fname=directory + 'final_dream.png')