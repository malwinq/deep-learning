import tensorflow as tf
import numpy as np

""" MNIST problem with convolutional network: added dropout and another stride """

# params
height = 28
width = 28
channels = 1
n_input = height * width
n_outputs = 10
n_epochs = 10
batch_size = 100

dropout_rate_conv2 = 0.25
dropout_rate_fc1 = 0.5

# initialize
with tf.name_scope('inputs'):
    x = tf.placeholder(tf.float32, shape=[None, n_input], name='x')
    x_reshaped = tf.reshape(x, shape=[-1, height, width, channels])
    y = tf.placeholder(tf.int32, shape=[None], name='y')
    train = tf.placeholder_with_default(False, shape=[], name='training')

conv1 = tf.layers.conv2d(x_reshaped, filters=32, kernel_size=3, strides=1,
                          padding='SAME', activation=tf.nn.relu, name='conv1')

conv2 = tf.layers.conv2d(conv1, filters=64, kernel_size=3, strides=1,
                         padding='SAME', activation=tf.nn.relu, name='conv2')

with tf.name_scope('pool3'):
    pool3 = tf.nn.max_pool(conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')
    pool3_flat = tf.reshape(pool3, shape=[-1, 64 * 7 * 7])
    pool3_flat_dropout = tf.layers.dropout(pool3_flat, dropout_rate_conv2, training=train)

with tf.name_scope('fc1'):
    fc1 = tf.layers.dense(pool3_flat, 64, activation=tf.nn.relu, name='fc1')
    fc1_dropout = tf.layers.dropout(fc1, dropout_rate_fc1, training=train)

with tf.name_scope('ooutput'):
    logits = tf.layers.dense(fc1, n_outputs, name='output')
    y_proba = tf.nn.softmax(logits, name='y_proba')

with tf.name_scope('train'):
    xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=y)
    loss = tf.reduce_mean(xentropy)
    optimizer = tf.train.AdamOptimizer()
    training_op = optimizer.minimize(loss)

with tf.name_scope('eval'):
    correct = tf.nn.in_top_k(logits, y, 1)
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

with tf.name_scope('init_and_save'):
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

# load the data
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
x_train = x_train.astype(np.float32).reshape(-1, 28*28) / 255.0
x_test = x_test.astype(np.float32).reshape(-1, 28*28) / 255.0
y_train = y_train.astype(np.int32)
y_test = y_test.astype(np.int32)

x_valid, x_train = x_train[:5000], x_train[5000:]
y_valid, y_train = y_train[:5000], y_train[5000:]


def shuffle_batch(x, y, batch_size):
    rand_ind = np.random.permutation(len(x))
    n_batches = len(x) // batch_size
    for batch_ind in np.array_split(rand_ind, n_batches):
        x_batch, y_batch = x[batch_ind], y[batch_ind]
        yield x_batch, y_batch


with tf.Session() as sess:
    init.run()
    for epoch in range(n_epochs):
        for x_batch, y_batch in shuffle_batch(x_train, y_train, batch_size):
            sess.run(training_op, feed_dict={x: x_batch, y: y_batch})
        acc_batch = accuracy.eval(feed_dict={x: x_batch, y: y_batch})
        acc_test = accuracy.eval(feed_dict={x: x_test, y: y_test})
        print(epoch, 'batch accuracy:', acc_batch, 'test accuracy:', acc_test)

        save_path = saver.save(sess, './my_mnist_model')























