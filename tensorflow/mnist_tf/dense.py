import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np

""" MNIST by neural network in tensorflow """


def neuron_layer(x, n_neurons, name, activation=None):
    """ Does the same as tf.layers.dense """
    with tf.name_scope(name):
        n_inputs = int(x.get_shape()[1])
        stddev = 2 / np.sqrt(n_inputs)
        init = tf.truncated_normal((n_inputs, n_neurons), stddev=stddev)        # init with gaussian distribution
        W = tf.Variable(init, name='kernel')
        b = tf.Variable(tf.zeros([n_neurons]), name='bias')
        Z = tf.matmul(x, W) + b
        if activation is not None:
            return activation(Z)
        else:
            return Z


# params
n_inputs = 28*28
n_hidden1 = 300
n_hidden2 = 100
n_outputs = 10
lr = 0.01

x = tf.placeholder(tf.float32, shape=(None, n_inputs), name='x')
y = tf.placeholder(tf.int64, shape=(None), name='y')

with tf.name_scope('model'):
    hidden1 = neuron_layer(x, n_inputs, name='hidden1', activation=tf.nn.relu)
    hidden2 = neuron_layer(hidden1, n_hidden2, name='hidden2', activation=tf.nn.relu)
    logits = neuron_layer(hidden2, n_outputs, name='outputs')           # logits are oututs without activation func

"""
with tf.name_scope('model'):
    hidden1 = tf.layers.dense(x, n_inputs, name='hidden1', activation=tf.nn.relu)
    hidden2 = tf.layers.dense(hidden1, n_hidden2, name='hidden2', activation=tf.nn.relu)
    logits = tf.layers.dense(hidden2, n_outputs, name='outputs')
"""

with tf.name_scope('loss'):
    xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
    loss = tf.reduce_mean(xentropy, name='strata')

with tf.name_scope('learning'):
    optimizer = tf.train.GradientDescentOptimizer(lr)
    training_op = optimizer.minimize(loss)

with tf.name_scope('result'):
    correct = tf.nn.in_top_k(logits, y, 1)
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

init = tf.global_variables_initializer()
saver = tf.train.Saver()


mnist = input_data.read_data_sets('/tmp/dane/')

n_epochs = 40
batch_size = 50

with tf.Session() as sess:
    init.run()
    for epoch in range(n_epochs):
        for iteration in range(mnist.train.num_examples // batch_size):
            x_batch, y_batch = mnist.train.next_batch(batch_size)
            sess.run(training_op, feed_dict={x: x_batch, y: y_batch})
        acc_train = accuracy.eval(feed_dict={x: x_batch, y: y_batch})
        acc_test = accuracy.eval(feed_dict={x: mnist.validation.images,
                                            y: mnist.validation.labels})

        print(epoch, ' training accuracy: ', acc_train, ' validation accuracy: ', acc_test)

    save_path = saver.save(sess, './model.ckpt')
