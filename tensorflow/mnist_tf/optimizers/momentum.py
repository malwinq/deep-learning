import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

""" MNIST classifier by neural network with batch normalization and momentum """

n_inputs = 28 * 28
n_hidden1 = 300
n_hidden2 = 100
n_outputs = 10
n_epochs = 40
batch_size = 50
lr = 0.01
mnist = input_data.read_data_sets('/tmp/dane/')

x = tf.placeholder(tf.float32, shape=(None, n_inputs), name='x')
train = tf.placeholder_with_default(False, shape=(), name='training')
y = tf.placeholder(tf.int64, shape=(None), name='y')

with tf.name_scope('model'):
    hidden1 = tf.layers.dense(x, n_hidden1, name='hidden1')
    bn1 = tf.layers.batch_normalization(hidden1, training=train, momentum=0.9)
    bn1_act = tf.nn.elu(bn1)
    hidden2 = tf.layers.dense(x, n_hidden2, name='hidden2')
    bn2 = tf.layers.batch_normalization(hidden2, training=train, momentum=0.9)
    bn2_act = tf.nn.elu(bn2)
    logits_bn = tf.layers.dense(bn2_act, n_outputs, name='outputs')
    logits = tf.layers.batch_normalization(logits_bn, training=train, momentum=0.9)

extra_update = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

with tf.name_scope('loss'):
    xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
    loss = tf.reduce_mean(xentropy, name='strata')

with tf.name_scope('learning'):
    optimizer = tf.train.MomentumOptimizer(learning_rate=lr, momentum=0.9)
    training_op = optimizer.minimize(loss)

with tf.name_scope('result'):
    correct = tf.nn.in_top_k(logits_bn, y, 1)
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

init = tf.global_variables_initializer()
saver = tf.train.Saver()

with tf.Session() as sess:
    init.run()
    for epoch in range(n_epochs):
        for iteration in range(mnist.train.num_examples // batch_size):
            x_batch, y_batch = mnist.train.next_batch(batch_size)
            sess.run([training_op, extra_update], feed_dict={train: True, x: x_batch, y: y_batch})
        acc_val = accuracy.eval(feed_dict={x: mnist.test.images, y: mnist.test.labels})
        print(epoch, 'Validation accuracy: ', acc_val)
