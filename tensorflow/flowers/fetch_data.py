import sys
import tarfile
from six.moves import urllib
import os

flowers_url = 'http://download.tensorflow.org/example_images/flower_photos.tgz'
flowers_path = os.path.join('datasets', 'flowers')


def download_progress(count, block_size, total_size):
    percents = count * block_size * 100 // total_size
    sys.stdout.write('\rDownloading: {}%'.format(percents))
    sys.stdout.flush()


def fetch_flowers(url=flowers_url, path=flowers_path):
    if os.path.exists(flowers_path):
        return None
    os.makedirs(path, exist_ok=True)
    tar_path = os.path.join(path, 'flower_photos.tgz')
    urllib.request.urlretrieve(url, tar_path, reporthook=download_progress)
    flowers_tar = tarfile.open(tar_path)
    flowers_tar.extractall(path=path)
    flowers_tar.close()
    # os.remove(tar_path)


fetch_flowers()
