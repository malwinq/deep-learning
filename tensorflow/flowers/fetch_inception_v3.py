import sys
import tarfile
from six.moves import urllib
import os

""" Script for fetching the Inception v3 model """

models_url = 'http://download.tensorflow.org/models'
inception_url = models_url + '/inception_v3_2016_08_28.tar.gz'
inception_path = os.path.join('datasets', 'inception')
inception_checkpoint_path = os.path.join(inception_path, 'inception_v3.ckpt')


def download_progress(count, block_size, total_size):
    percents = count * block_size * 100 // total_size
    sys.stdout.write('\rDownloading: {}%'.format(percents))
    sys.stdout.flush()


def fetch_pretrained_inception_v3(url=inception_url, path=inception_path):
    if os.path.exists(inception_checkpoint_path):
        return None
    os.makedirs(path, exist_ok=True)
    tg_path = os.path.join(path, 'inception_v3.tgz')
    urllib.request.urlretrieve(url, tg_path, reporthook=download_progress)
    inception_tg = tarfile.open(tg_path)
    inception_tg.extractall(path=path)
    inception_tg.close()
    os.remove(tg_path)


fetch_pretrained_inception_v3()