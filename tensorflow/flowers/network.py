import os
import numpy as np
from collections import defaultdict
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from skimage.transform import resize

""" The neural network for flowers classification, using Inception v3"""

# params
width = 299
height = 299
channels = 3
zoom = 0.2


def parse_image(image, target_width=width, target_height=height, max_zoom=zoom):
    """ Preprocessing of images for data augumentation """
    h = image.shape[0]
    w = image.shape[1]
    image_ratio = w / h
    target_ratio = target_width / target_height
    crop_vert = image_ratio < target_ratio      # boolean var if it's possible to crop vertically
    crop_width = w if crop_vert else int(h * target_ratio)
    crop_height = int(w / target_ratio) if crop_vert else h
    resize_factor = np.random.rand() * max_zoom + 1.0
    crop_width = int(crop_width / resize_factor)
    crop_height = int(crop_height / resize_factor)

    # location of boundary box on the image
    x0 = np.random.randint(0, w - crop_width)
    y0 = np.random.randint(0, h - crop_height)
    x1 = x0 + crop_width
    y1 = y0 + crop_height

    image = image[y0:y1, x0:x1]

    if np.random.rand() < 0.5:
        image = np.fliplr(image)

    image = resize(image, (target_width, target_height))
    return image.astype(np.float32)


flowers_path = os.path.join('datasets', 'flowers')

# classes of flowers
flowers_path = os.path.join(flowers_path, 'flower_photos')
flower_classes = sorted([name for name in os.listdir(flowers_path) if os.path.isdir(os.path.join(flowers_path, name))])
print(flower_classes)

# create a list of paths to photos
image_paths = defaultdict(list)
for fl_class in flower_classes:
    image_path = os.path.join(flowers_path, fl_class)
    for path in os.listdir(image_path):
        if path.endswith('.jpg'):
            image_paths[fl_class].append(os.path.join(image_path, path))

for paths in image_paths.values():
    paths.sort()
    # print(paths)

""" 
# printing the flowers
n = 2
for fl_class in flower_classes:
    print('Class:', fl_class)
    plt.figure(figsize=(10, 5))
    for index, example_path in enumerate(image_paths[fl_class][:n]):
        example_image = mpimg.imread(example_path)[:, :, :channels]
        plt.subplot(100 + n * 10 + index + 1)
        plt.title('{}x{}'.format(example_image.shape[1], example_image.shape[0]))
        plt.imshow(example_image)
        plt.axis('off')
    plt.show()
"""
"""
# printing the cropped and flipped photos
n = 2
for fl_class in flower_classes:
    print('Class:', fl_class)
    for index, example_path in enumerate(image_paths[fl_class][:n]):
        example_image = mpimg.imread(example_path)[:, :, :channels]
        example_image = parse_image(example_image)
        plt.figure(figsize=(6, 8))
        plt.title('{}x{}'.format(example_image.shape[1], example_image.shape[0]))
        plt.imshow(example_image)
        plt.axis('off')
    plt.show()
"""


