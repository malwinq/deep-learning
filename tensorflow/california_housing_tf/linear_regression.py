import tensorflow as tf
import numpy as np
from sklearn.datasets import fetch_california_housing

""" California housing with tensorflow, linear regression """

housing = fetch_california_housing()
m, n = housing.data.shape
housing_data_bias = np.c_[np.ones((m, 1), housing.data)]

x = tf.constant(housing_data_bias, dtype=tf.float32, name='x')
y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name='y')
xt = tf.transpose(x)
theta = tf.matmul(tf.matmul(tf.matrix_inverse(tf.matmul(xt, x)), xt), y)

with tf.Session() as sess:
    theta_value = theta.eval()
    print(theta_value)
