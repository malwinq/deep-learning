# Deep Learning

Various projects with AI/ML algorithms (neural networks and others).

## Keras
* weather prediction
* generating poems
* DeepDream
* boston housing
* dogs vs cats
* IMDb rates classification
* MNIST
* reuters agency

## TensorFlow
* flowers classification (uses Inception v3)
* MNIST
* california housing

## Scikit-learn
* california housing
* spam classifier (using NLTK)
* Iris (flowers) classifier
* MNIST
* titanic
* money and happiness
* dimensionality reduction

## PyTorch
* CIFAR-10
* MNIST

### Other info

Additional libraries: NumPy, pandas, matplotlib, OpenCV, SciPy, NTLK.

To run TensorBoard views type
```
tensorboard --logdir=log
```
I use GPUs from Google Colab and Jupyter Notebooks.
