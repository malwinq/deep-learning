from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from parse_data import *
from attributes_adder import *

""" Cleans data by removing unused columns and adding new ones """

# loading data
df = load_data()
df_with_id = df.reset_index()

# splitting income into categories
df['income_cat'] = np.ceil(df['median_income'] / 1.5)
df['income_cat'].where(df['income_cat'] < 5, 5.0, inplace=True)

# strata sampling
split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
for train_in, test_in in split.split(df, df['income_cat']):
    strat_train = df.loc[train_in]
    strat_test = df.loc[test_in]
print('Splitting done')

print(df['income_cat'].value_counts() / len(df))

# deleting column income_cat
for strat_set in (strat_test, strat_train):
    strat_set.drop('income_cat', axis=1, inplace=True)

print('Training: ', len(strat_train), ' Test: ', len(strat_test))

# splitting data to inputs and outputs
data = strat_train.drop('median_house_value', axis=1)
data_labels = strat_train['median_house_value'].copy()

# cleaning the data
imputer = SimpleImputer(strategy='median')
data_num = data.drop('ocean_proximity', axis=1)
imputer.fit(data_num)
X = imputer.transform(data_num)

data_tr = pd.DataFrame(X, columns=data_num.columns)

# encode ocean_proximity
encoder = LabelEncoder()
data_cat = data['ocean_proximity']
data_cat_encoded = encoder.fit_transform(data_cat)

encoder = OneHotEncoder()
data_cat_1hot = encoder.fit_transform(data_cat_encoded.reshape(-1, 1))

print('Cleaning done')
