from parse_data import *
import matplotlib.pyplot as plt

df = load_data()

print('HEAD')
print(df.head())

print('INFO')
print(df.info())

print('OCEAN PROXIMITY')
print(df['ocean_proximity'].value_counts())

print('DESCRIBE')
print(df.describe())

df.hist(bins=50, figsize=(30, 30))
plt.show()

train, test = split_data(df, 0.2)
print('Training: ', len(train),' Test: ', len(test))