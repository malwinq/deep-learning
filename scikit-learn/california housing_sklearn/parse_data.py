import os
import tarfile
from six.moves import urllib
import pandas as pd
import numpy as np
import hashlib

h_url = "https://github.com/ageron/handson-ml/blob/master/datasets/housing/housing.tgz"
h_path = r'/home/malwina/repo/deep-learning/california housing/dataset'


def fetch_data(ho_url=h_url, ho_path=h_path):
    if not os.path.isdir(ho_path):
        os.makedirs(ho_path)
    tgz_path = os.path.join(ho_path, 'housing.tgz')
    urllib.request.urlretrieve(ho_url, tgz_path)
    ho_tgz = tarfile.open(tgz_path)
    ho_tgz.extractall(path=ho_path)
    ho_tgz.close()


def load_data(ho_path=h_path):
    csv_path = os.path.join(ho_path, 'housing.csv')
    housing = pd.read_csv(csv_path)
    return housing


def split_data(data, ratio):
    """ Split dataset for train and validation """
    shuffled = np.random.permutation(len(data))
    test_size = int(len(data) * ratio)
    test_indicies = shuffled[:test_size]
    train_indicies = shuffled[test_size:]
    return data.iloc[train_indicies], data.iloc[test_indicies]


def test_check(identifier, ratio, hash):
    return hash(np.int64(identifier)).digest()[-1] < 256 * ratio


def split_by_id(data, ratio, id_col, hash=hashlib.md5):
    ids = data[id_col]
    in_test = ids.apply(lambda id: test_check(id, ratio, hash))
    return data.loc[~in_test], data.loc[in_test]