from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin


class DataFrameSelector(BaseEstimator, TransformerMixin):
    """ Class for selecting data from dataframe """

    def __init__(self, att_names):
        self.att_names = att_names

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        return x[self.att_names].values
