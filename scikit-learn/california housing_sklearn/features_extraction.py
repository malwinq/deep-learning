from sklearn.model_selection import StratifiedShuffleSplit
import matplotlib.pyplot as plt
from pandas.plotting import scatter_matrix
# from sklearn.model_selection import train_test_split
from parse_data import *

""" Script for visualising the features and its correlations to extract important data """

# loading data
df = load_data()
df_with_id = df.reset_index()

# train_set, test_set = train_test_split(df, test_size=0.2, random_state=42)
# train_set, test_set = split_by_id(df_with_id, 0.2, 'index')

# splitting income into categories
df['income_cat'] = np.ceil(df['median_income'] / 1.5)
df['income_cat'].where(df['income_cat'] < 5, 5.0, inplace=True)

# strata sampling
split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
for train_in, test_in in split.split(df, df['income_cat']):
    strat_train = df.loc[train_in]
    strat_test = df.loc[test_in]
print('Splitting done')

print(df['income_cat'].value_counts() / len(df))

# deleting column income_cat
for strat_set in (strat_test, strat_train):
    strat_set.drop('income_cat', axis=1, inplace=True)

print('Training: ', len(strat_train), ' Test: ', len(strat_test))

# visualize data
data = strat_train.copy()           # copy of original data
data.plot(kind='scatter', x='longitude', y='latitude', alpha=0.1)

data.plot(kind='scatter', x='longitude', y='latitude', alpha=0.4,
          s=data['population']/100, label='Population', figsize=(10, 7),
          c='median_house_value', cmap=plt.get_cmap('jet'), colorbar=True)
plt.legend()

# correlation
attributes = ['median_house_value', 'median_income', 'total_rooms', 'housing_median_age']
scatter_matrix(data[attributes], figsize=(30, 30))

data.plot(kind='scatter', x='median_income', y='median_house_value', alpha=0.1)

# new data
data['rooms_per_family'] = data['total_rooms'] / data['households']
data['bedrooms_per_rooms'] = data['total_bedrooms'] / data['total_rooms']
data['population_per_family'] = data['population'] / data['households']

data_corr = data.corr()
print(data_corr['median_house_value'].sort_values(ascending=False))

plt.show()
