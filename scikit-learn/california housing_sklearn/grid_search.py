from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error

from parse_data import *
from attributes_adder import *

""" Training random forest model with 5-fold cross-validation and grid search -> 47k rmse """

# --------------------------- DATA PREPARATION ----------------------------------
# loading data
df = load_data()
df_with_id = df.reset_index()

# splitting income into categories
df['income_cat'] = np.ceil(df['median_income'] / 1.5)
df['income_cat'].where(df['income_cat'] < 5, 5.0, inplace=True)

# strata sampling
split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
for train_in, test_in in split.split(df, df['income_cat']):
    strat_train = df.loc[train_in]
    strat_test = df.loc[test_in]
print('Splitting done')

print(df['income_cat'].value_counts() / len(df))

# deleting column income_cat
for strat_set in (strat_test, strat_train):
    strat_set.drop('income_cat', axis=1, inplace=True)

print('Training: ', len(strat_train), ' Test: ', len(strat_test))

# splitting data to inputs and outputs
data = strat_train.drop('median_house_value', axis=1)
data_labels = strat_train['median_house_value'].copy()

data_num = data.drop('ocean_proximity', axis=1)

num_att = list(data_num)
cat_att = ['ocean_proximity']

# scikit-learn pipelines for feature scaling
# for numeric data
pipeline_num = Pipeline([
    ('imputer', SimpleImputer(strategy='median')),
    ('att_adder', CombinedAttributesAdder()),
    ('std_scaler', StandardScaler()),
])

# full pipeline
pipeline_full = ColumnTransformer([
    ("num", pipeline_num, num_att),
    ("cat", OneHotEncoder(), cat_att),
])

# ready data
prepared_data = pipeline_full.fit_transform(data)
print('Shape: ', prepared_data.shape)

# --------------------------- TRAINING MODEL ----------------------------------
forest = RandomForestRegressor()

grid = [
    {'n_estimators': [70, 80], 'max_features': [4, 6, 8]},
    {'bootstrap': [False], 'n_estimators': [30, 40], 'max_features': [5, 6]},
]

# 5-fold cross-validation
search = GridSearchCV(forest, grid, cv=5, scoring='neg_mean_squared_error', return_train_score=True)

search.fit(prepared_data, data_labels)

print('Best params: ', search.best_params_)
print('Best estimator: ', search.best_estimator_)

scores = search.cv_results_
for mean, params in zip(scores['mean_test_score'], scores['params']):
    print(np.sqrt(-mean), params)

final_model = search.best_estimator_

# predictions on test set
x_test = strat_test.drop('median_house_value', axis=1)
y_test = strat_test['median_house_value'].copy()
x_test_prepared = pipeline_full.transform(x_test)
final_pred = final_model.predict(x_test_prepared)

final_mse = mean_squared_error(y_test, final_pred)
final_rmse = np.sqrt(final_mse)
print('FINAL SCORE: ', final_rmse)

print('Done')
