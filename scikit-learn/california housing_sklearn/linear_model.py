from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score

from parse_data import *
from attributes_adder import *

""" Training linear model with 10-fold cross-validation """

# --------------------------- DATA PREPARATION ----------------------------------
# loading data
df = load_data()
df_with_id = df.reset_index()

# splitting income into categories
df['income_cat'] = np.ceil(df['median_income'] / 1.5)
df['income_cat'].where(df['income_cat'] < 5, 5.0, inplace=True)

# strata sampling
split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
for train_in, test_in in split.split(df, df['income_cat']):
    strat_train = df.loc[train_in]
    strat_test = df.loc[test_in]
print('Splitting done')

print(df['income_cat'].value_counts() / len(df))

# deleting column income_cat
for strat_set in (strat_test, strat_train):
    strat_set.drop('income_cat', axis=1, inplace=True)

print('Training: ', len(strat_train), ' Test: ', len(strat_test))

# splitting data to inputs and outputs
data = strat_train.drop('median_house_value', axis=1)
data_labels = strat_train['median_house_value'].copy()

data_num = data.drop('ocean_proximity', axis=1)

num_att = list(data_num)
cat_att = ['ocean_proximity']

# scikit-learn pipelines for feature scaling
# for numeric data
pipeline_num = Pipeline([
    # ('selector', DataFrameSelector(num_att)),
    ('imputer', SimpleImputer(strategy='median')),
    ('att_adder', CombinedAttributesAdder()),
    ('std_scaler', StandardScaler()),
])

# full pipeline
pipeline_full = ColumnTransformer([
    ("num", pipeline_num, num_att),
    ("cat", OneHotEncoder(), cat_att),
])

# ready data
prepared_data = pipeline_full.fit_transform(data)
print('Shape: ', prepared_data.shape)

# --------------------------- TRAINING MODEL ----------------------------------
linear = LinearRegression()

scores = cross_val_score(linear, prepared_data, data_labels,
                         scoring='neg_mean_squared_error', cv=10)
scores_rmse = np.sqrt(-scores)

print('Results: ', scores_rmse)
print('Average: ', scores_rmse.mean())
print('Standard deviation: ', scores_rmse.std())

print('Done')
