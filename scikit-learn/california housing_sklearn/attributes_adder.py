from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
import numpy as np

rooms_ind = 3
bedrooms_ind = 4
population_ind = 5
household_ind = 6


class CombinedAttributesAdder(BaseEstimator, TransformerMixin):
    """ Add new columns to dataframe """

    def __init__(self, add_bedr_per_room=True):
        self.add_bedr_per_room = add_bedr_per_room

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None):
        rooms_per_family = x[:, rooms_ind] / x[:, household_ind]
        population_per_family = x[:, population_ind] / x[:, household_ind]
        if self.add_bedr_per_room:
            bedrooms_per_rooms = x[:, bedrooms_ind] / x[:, rooms_ind]
            return np.c_[x, rooms_per_family, population_per_family, bedrooms_per_rooms]
        else:
            return np.c_[x, rooms_per_family, population_per_family]
