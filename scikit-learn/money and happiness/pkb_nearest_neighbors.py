import sklearn
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

""" Check the relation between PKB per capita and people's happiness.
    The method of k-nearest neighbors. """

# loading data
oecd_bli = pd.read_csv('oecd_bli_2015.csv', thousands=',')
gdp_per_capita = pd.read_csv('gdb_per_capita.csv', thousands=',',
                             delimeter='\t', encoding='latin1',
                             na_values='bd')

# data preparation
country_stats = prepare_country_stats(oecd_bli, gdp_per_capita)     # function in module
x = np.c_[country_stats['PKB per capita']]
y = np.c_[country_stats['Life satisfaction']]

# visualize the data
country_stats.plot(kind='scatter', x='PKB per capita', y='Life satisfaction')
plt.show()

# linear model
model = sklearn.neighbors.KNeighborsRegressor(n_neighbors=3)

# training
model.fit(x, y)

# prediction
x_new = [[22587]]       # Cyprus
print(model.predict(x_new))
