import os
import tarfile
import urllib.request

url = 'http://spamassassin.apache.org/old/publiccorpus/'
ham_url = url + '20030228_easy_ham.tar.bz2'
spam_url = url + '20030228_spam.tar.bz2'
path = r'/home/malwina/repo/deep-learning/scikit-learn/spam_classifier/dataset/'

ham_name = 'ham.tar.bz2'
spam_name = 'spam.tar.bz2'

ham_path = path + ham_name
spam_path = path + spam_name

urllib.request.urlretrieve(ham_url, ham_path)
urllib.request.urlretrieve(spam_url, spam_path)

tar_ham_file = tarfile.open(ham_path)
tar_spam_file = tarfile.open(spam_path)

tar_ham_file.extractall(path=path)
tar_spam_file.extractall(path=path)

tar_ham_file.close()
tar_spam_file.close()
