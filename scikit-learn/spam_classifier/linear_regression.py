import nltk
import os
import email
import email.policy
import email.parser
from collections import Counter
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from scipy.sparse import csr_matrix
import re
from html import unescape
import urlextract

""" Classifier of spam emails using LogisticRegression """

# params
path = r'/home/malwina/repo/deep-learning/scikit-learn/spam_classifier/dataset/'
ham_path = path + 'easy_ham'
spam_path = path + 'spam'


class EmailToWordCounter(BaseEstimator, TransformerMixin):
    """ Transformer emails to words counter """
    def __init__(self, strip_headers=True, lower_case=True, remove_punctuation=True,
                 replace_urls=True, replace_numbers=True, stemming=True):
        self.strip_headers = strip_headers
        self.lower_case = lower_case
        self.remove_punctuation = remove_punctuation
        self.replace_urls = replace_urls
        self.replace_numbers = replace_numbers
        self.stemming = stemming

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X_transformed = []
        for email in X:
            text = email_to_text(email) or ''
            if self.lower_case:
                text = text.lower()
            if self.replace_urls and url_extractor is not None:
                urls = list(set(url_extractor.find_urls(text)))
                urls.sort(key=lambda url: len(url), reverse=True)
                for url in urls:
                    text = text.replace(url, ' URL ')
            if self.replace_numbers:
                text = re.sub(r'\d+(?:\.\d*(?:[eE]\d+))?', 'NUMBER', text)
            if self.remove_punctuation:
                text = re.sub(r'\W+', ' ', text, flags=re.M)
            word_counts = Counter(text.split())
            if self.stemming and stemmer is not None:
                stemmed_word_counts = Counter()
                for word, count in word_counts.items():
                    stemmed_word = stemmer.stem(word)
                    stemmed_word_counts[stemmed_word] += count
                word_counts = stemmed_word_counts
            X_transformed.append(word_counts)
        return np.array(X_transformed)


class WordCounterToVector(BaseEstimator, TransformerMixin):
    """ Transformer words counts to vectors """
    def __init__(self, vocab_size=1000):
        self.vocab_size = vocab_size

    def fit(self, X, y=None):
        total_count = Counter()
        for word_count in X:
            for word, count in word_count.items():
                total_count[word] += min(count, 10)
        most_common = total_count.most_common()[:self.vocab_size]
        self.most_common = most_common
        self.vocabulary = {word: index + 1 for index, (word, count) in enumerate(most_common)}
        return self

    def transform(self, X, y=None):
        rows = []
        cols = []
        data = []
        for row, word_count in enumerate(X):
            for word, count in word_count.items():
                rows.append(row)
                cols.append(self.vocabulary.get(word, 0))
                data.append(count)
        return csr_matrix((data, (rows, cols)), shape=(len(X), self.vocab_size + 1))


def load_email(is_spam, filename):
    f_path = spam_path if is_spam else ham_path
    f_path = f_path + '/' + filename
    with open(f_path, 'rb') as f:
        return email.parser.BytesParser(policy=email.policy.default).parse(f)


def get_email_structure(email):
    if isinstance(email, str):
        return email
    payload = email.get_payload()
    if isinstance(payload, list):
        return 'multipart({})'.format(', '.join([get_email_structure(sum_email) for sum_email in payload]))
    else:
        return email.get_content_type()


def structures_counter(emails):
    structures = Counter()
    for email in emails:
        structure = get_email_structure(email)
        structures[structure] += 1
    return structures


def html_to_text(html):
    text = re.sub('<head.*?>.*?</head>', '', html, flags=re.M | re.S | re.I)
    text = re.sub('<a\s.*?>', ' HYPERLINK ', text, flags=re.M | re.S | re.I)
    text = re.sub('<.*?>', '', text, flags=re.M | re.S)
    text = re.sub(r'(\s*\n)+', '\n', text, flags=re.M | re.S)
    return unescape(text)


def email_to_text(email):
    html = None
    for part in email.walk():
        ctype = part.get_content_type()
        if not ctype in ('text/plain', 'text/html'):
            continue
        try:
            content = part.get_content()
        except:
            content = str(part.get_payload())
        if ctype == 'text/plain':
            return content
        else:
            html = content
    if html:
        return html_to_text(html)


# load data
ham_filenames = [name for name in sorted(os.listdir(ham_path)) if len(name) > 20]
spam_filenames = [name for name in sorted(os.listdir(spam_path)) if len(name) > 20]

print('Ham files: ', len(ham_filenames))
print('Spam files: ', len(spam_filenames))

# read data
ham_emails = [load_email(is_spam=False, filename=name) for name in ham_filenames]
spam_emails = [load_email(is_spam=True, filename=name) for name in spam_filenames]

# print(ham_emails[1].get_content().strip())
# print(structures_counter(spam_emails).most_common())

# splitting x and y and test and train data
X = np.array(ham_emails + spam_emails)
y = np.array([0] * len(ham_emails) + [1] * len(spam_emails))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# parsing emails
html_spam_emails = [email for email in X_train[y_train == 1] if get_email_structure(email) == 'text/html']
sample_html_spam = html_spam_emails[7]
# print(sample_html_spam.get_content().strip()[:1000], '...')
# print(html_to_text(sample_html_spam.get_content())[:1000], '...')
# print(email_to_text(sample_html_spam)[:100], '...')

# stemming of words
stemmer = nltk.PorterStemmer()

url_extractor = urlextract.URLExtract()

# X_few = X_train[:3]
# X_few_wordcounts = EmailToWordCounter().fit_transform(X_few)
# print(X_few_wordcounts)

# vocab_transformer = WordCounterToVector(vocab_size=10)
# X_few_vector = vocab_transformer.fit_transform(X_few_wordcounts)
# print(X_few_vectors)

# preprocess dataset
preprocess_pipeline = Pipeline([
    ('email_to_wordcount', EmailToWordCounter()),
    ('wordcount_to_vector', WordCounterToVector()),
])

X_train_transformed = preprocess_pipeline.fit_transform(X_train)
X_test_transformed = preprocess_pipeline.fit_transform(X_test)

# train model
log_clf = LogisticRegression(solver="lbfgs", random_state=42)
score = cross_val_score(log_clf, X_train_transformed, y_train, cv=3, verbose=3)
print(score.mean())
