from sklearn.datasets import fetch_openml
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib as mpl
import matplotlib.pyplot as plt

""" Binary classification by random forest - 5s and not-5s, plots with useful metrics"""


def plot_digit(digit):
    """ Plotting digits with matplotlib """
    image = digit.reshape(28, 28)
    plt.imshow(image, cmap=mpl.cm.binary, interpolation='nearest')
    plt.axis("off")


def plot_precision_recall_vs_treshold(precisions, recalls, threshold):
    """ Plotting precision and recall in threshold domain """
    plt.plot(threshold, precisions[:-1], 'b--', label='Precision')
    plt.plot(threshold, recalls[:-1], 'g-', label='Recall')
    plt.xlabel('Threshold')
    plt.legend(loc='center left')
    plt.ylim([0, 1])


def plot_roc_curve(fpr, tpr, label=None):
    """ Plotting the receiver operating characteristics """
    plt.plot(fpr, tpr, linewidth=2, label=label)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.axis([0, 1, 0, 1])
    plt.xlabel('False Positive Rate (Fall-Out)')
    plt.ylabel('True Positive Rate (Recall)')
    plt.grid(True)


# download data
mnist = fetch_openml('mnist_784', version=1)
print('Download completed')

x, y = mnist['data'], mnist['target']
y = y.astype(np.uint8)

# splitting data
x_train, x_test = x[:60000], x[60000:]
y_train, y_test = y[:60000], y[60000:]

shuffle = np.random.permutation(60000)
x_train, y_train = x_train[shuffle], y_train[shuffle]

y_train_5 = (y_train == 5)
y_test_5 = (y_test == 5)

# training the model
forest = RandomForestClassifier(n_estimators=100, random_state=42)

# k-fold cross validation
y_train_pred = cross_val_predict(forest, x_train, y_train_5, cv=3, method='predict_proba')
y_scores = y_train_pred[:, 1]

# plots
precision, recall, threshold = precision_recall_curve(y_train_5, y_scores)
plot_precision_recall_vs_treshold(precision, recall, threshold)
plt.show()

# roc curve
fpr, tpr, threshold = roc_curve(y_train_5, y_scores)
plot_roc_curve(fpr, tpr, threshold)
plt.show()

# area under the curve
print('AUC: ', roc_auc_score(y_train_5, y_scores))

