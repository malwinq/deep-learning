from sklearn.datasets import fetch_openml
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

""" MNIST classification by support vector machine with gaussian rbf kernel """

# download data
mnist = fetch_openml('mnist_784', version=1)

x, y = mnist['data'], mnist['target']
y = y.astype(np.uint8)

# splitting data
x_train, x_test = x[:60000], x[60000:]
y_train, y_test = y[:60000], y[60000:]

shuffle = np.random.permutation(60000)
x_train, y_train = x_train[shuffle], y_train[shuffle]

# preprocess of data and classifier
svm_classifier = Pipeline([
    ('scalar', StandardScaler()),
    ('linear_svc', SVC(kernel='rbf', gamma=5, C=0.01))        # faster than SVC(kernel='linear')
])

svm_classifier.fit(x_train, y_train)

pred = svm_classifier.predict(x_test[100])
print(pred)
