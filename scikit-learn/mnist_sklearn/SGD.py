from sklearn.datasets import fetch_openml
import numpy as np
from sklearn.linear_model import SGDClassifier

""" MNIST classification by SGD """

# download data
mnist = fetch_openml('mnist_784', version=1)
print(mnist.keys())

x, y = mnist['data'], mnist['target']
y = y.astype(np.uint8)

# splitting data
x_train, x_test = x[:60000], x[60000:]
y_train, y_test = y[:60000], y[60000:]

shuffle = np.random.permutation(60000)
x_train, y_train = x_train[shuffle], y_train[shuffle]

# training the model
sgd = SGDClassifier(max_iter=1000, tol=1e-3, random_state=42)
sgd.fit(x_train, y_train)
