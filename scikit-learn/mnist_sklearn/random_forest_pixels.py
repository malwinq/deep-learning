from sklearn.datasets import fetch_openml
import numpy as np
from sklearn.ensemble import RandomForestClassifier
import matplotlib as mpl
import matplotlib.pyplot as plt

""" Classification by random forest with plotting the importance of pixels """


def plot_pixels(data):
    image = data.reshape(28, 28)
    plt.imshow(image, cmap=mpl.cm.hot, interpolation='nearest')
    plt.axis('off')

    bar = plt.colorbar(ticks=[data.min(), data.max()])
    bar.ax.set_yticklabels(['Not important', 'Very important'])
    plt.show()


# download data
mnist = fetch_openml('mnist_784', version=1)
print('Download completed')

x, y = mnist['data'], mnist['target']
y = y.astype(np.uint8)

# training the model
forest_classifier = RandomForestClassifier(n_estimators=100, random_state=42)
forest_classifier.fit(x, y)

plot_pixels(forest_classifier.feature_importances_)
