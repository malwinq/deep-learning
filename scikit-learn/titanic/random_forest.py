import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
from sklearn.preprocessing import OneHotEncoder
from sklearn.pipeline import FeatureUnion
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier

""" Predicting the probability of surviving the titanic cruise - random forest, 81% """

# params
data_path = r'/home/malwina/repo/deep-learning/scikit-learn/titanic/dataset/'


class DataFrameSelector(BaseEstimator, TransformerMixin):
    """ Class for selecting data from dataframe """

    def __init__(self, att_names):
        self.att_names = att_names

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        return x[self.att_names]


class MostFrequentImputer(BaseEstimator, TransformerMixin):
    """ Transform string columns """
    def fit(self, X):
        self.most_frequent_ = pd.Series([X[c].value_counts().index[0] for c in X], index=X.columns)
        return self

    def transform(self, X):
        return X.fillna(self.most_frequent_)


def load_data(filename, path=data_path):
    return pd.read_csv(path + filename)


# loading data
train_data = load_data('train.csv')
test_data = load_data('test.csv')

# show the info
print(train_data.head())
print(train_data.describe())
print(train_data.info())
print(train_data['Survived'].value_counts())

# pipelines for preprocessing data (num for numeric, cat for strings)
num_pipeline = Pipeline([
    ('select_numeric', DataFrameSelector(['Age', 'SibSp', 'Parch', 'Fare'])),
    ('imputer', SimpleImputer(strategy='median')),
])

cat_pipeline = Pipeline([
    ('select_cat', DataFrameSelector(['Pclass', 'Sex', 'Embarked'])),
    ('imputer', MostFrequentImputer()),
    ('cat_encoder', OneHotEncoder(sparse=False)),
])

preprocess_pipeline = FeatureUnion(transformer_list=[
    ('num_pipeline', num_pipeline),
    ('cat_pipeline', cat_pipeline),
])

X_train = preprocess_pipeline.fit_transform(train_data)
y_train = train_data['Survived']

# train model
forest_clf = RandomForestClassifier(n_estimators=100, random_state=42)
forest_scores = cross_val_score(forest_clf, X_train, y_train, cv=10)

# make predictions
X_test = preprocess_pipeline.transform(test_data)
y_pred = forest_clf.predict(X_test)

# score
forest_scores = cross_val_score(forest_clf, X_train, y_train, cv=10)
print(forest_scores.mean())
