import sys
import sklearn
import numpy as np
import os
import matplotlib as mpl
import matplotlib.pyplot as plt

""" Training the dimensionality reduction with 3D dataset, using numpy """

# create 3d data
np.random.seed(4)
m = 60
noise = 0.1

angles = np.random.rand(m) * 3 * np.pi / 2 - 0.5
X = np.empty((m, 3))
X[:, 0] = np.cos(angles) + np.sin(angles)/2 + noise * np.random.randn(m) / 2
X[:, 1] = np.sin(angles) * 0.7 + noise * np.random.randn(m) / 2
X[:, 2] = X[:, 0] * 0.1 + X[:, 1] * 0.3 + noise * np.random.randn(m)

X_centr = X - X.mean(axis=0)
U, s, Vt = np.linalg.svd(X_centr)

# splitting data to two components
c1 = Vt.T[:, 0]
c2 = Vt.T[:, 1]

print('c1: ', c1)
print('c2: ', c2)
