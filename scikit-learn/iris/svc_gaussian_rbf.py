import numpy as np
from sklearn import datasets
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

""" Classifier of iris flowers using SVC and gaussian RBF kernel (kernel trick) """

# params
C = 0.001       # determining the width of "border"

# load data
iris = datasets.load_iris()
X = iris['data'][:, (2, 3)]
y = (iris['target'] == 2).astype(np.float64)       # load only 2 from 3 types of flowers

# preprocess of data and classifier
svm_classifier = Pipeline([
    ('scalar', StandardScaler()),
    ('linear_svc', SVC(kernel='rbf', gamma=5, C=C))        # faster than SVC(kernel='linear')
])

svm_classifier.fit(X, y)

# prediction
pred = svm_classifier.predict([[5.5, 1.7]])
print(pred)
