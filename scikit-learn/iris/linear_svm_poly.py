import numpy as np
from sklearn import datasets
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from sklearn.preprocessing import PolynomialFeatures

""" Classifier of iris flowers using LinearSVC with polynomial features, param C = 10 and hinge loss """

# params
C = 10       # determining the width of "border"

# load data
iris = datasets.load_iris()
X = iris['data'][:, (2, 3)]
y = (iris['target'] == 2).astype(np.float64)       # load only 2 from 3 types of flowers

# preprocess of data and classifier
svm_classifier = Pipeline([
    ('poly_features', PolynomialFeatures(degree=3)),
    ('scalar', StandardScaler()),
    ('linear_svc', LinearSVC(C=C, loss='hinge'))        # faster than SVC(kernel='linear')
])

svm_classifier.fit(X, y)

# prediction
pred = svm_classifier.predict([[5.5, 1.7]])
print(pred)
