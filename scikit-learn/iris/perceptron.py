import numpy as np
from sklearn.datasets import load_iris
from sklearn.linear_model import Perceptron

""" Iris classifier by simple perceptron"""

data = load_iris()
x = data.data[:, (2, 3)]
y = (data.target == 0).astype(np.int)
clf = Perceptron(random_state=42)
clf.fit(x, y)

pred = clf.predict([[1.5, 0.7]])
print(pred)
