from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier

""" Training random forest classifier and checking the importance of features """

dataset = load_iris()
x = dataset['data']
y = dataset['target']
names = dataset['feature_names']
forest_classifier = RandomForestClassifier(n_estimators=500, n_jobs=-1)
forest_classifier.fit(x, y)

for name, score in zip(names, forest_classifier.feature_importances_):
    print(name, score)
