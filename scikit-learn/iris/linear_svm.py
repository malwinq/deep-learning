import numpy as np
from sklearn import datasets
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC

""" Classifier of iris flowers using LinearSVR with param C = 1 and hinge loss """

# params
C = 1       # determining the width of "border"

# load data
iris = datasets.load_iris()
X = iris['data'][:, (2, 3)]
y = (iris['target'] == 2).astype(np.float64)       # load only 2 from 3 types of flowers

# preprocess of data and classifier
svm_classifier = Pipeline([
    ('scalar', StandardScaler()),
    ('linear_svc', LinearSVC(C=C, loss='hinge'))        # faster than SVC(kernel='linear')
])

svm_classifier.fit(X, y)

# prediction
pred = svm_classifier.predict([[5.5, 1.7]])
print(pred)
