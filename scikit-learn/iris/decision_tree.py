from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz

""" Iris classifier with decision tree """

# load data
dataset = load_iris()
x = dataset.data[:, 2:]
y = dataset.target

# tree classifier
tree = DecisionTreeClassifier(max_depth=2)
tree.fit(x, y)

# visualize the graph
export_graphviz(
    tree,
    out_file='iris_tree.dot',
    feature_names=dataset.feature_names[2:],
    class_names=dataset.target_names,
    rounded=True,
    filled=True
)
# to see graph in .png format:
# $ dot -Tpng iris_tree.dot -o iris_tree.png
