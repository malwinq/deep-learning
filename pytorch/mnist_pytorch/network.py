import torch
import torchvision
from torch import nn
import torch.nn.functional as F
import torchvision.transforms as transforms


class Net(nn.Module):
    """ Class of neural network layers """

    def __init__(self):
        # defining superclass
        super(Net, self).__init__()

        # convolution layers
        self.conv1 = nn.Conv2d(1, 64, kernel_size=(3, 3), padding=1)
        self.conv2 = nn.Conv2d(64, 64,kernel_size=(3, 3), padding=1)

        # pooling
        self.max_pool = nn.MaxPool2d(2, 2)
        self.global_pool = nn.AvgPool2d(7)

        # dense layers (fully-connected)
        self.fc1 = nn.Linear(64, 64)
        self.fc2 = nn.Linear(64, 10)

    def forward(self, x):
        """ Flow of data in network """
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = self.max_pool(x)

        x = F.relu(self.conv2(x))
        x = F.relu(self.conv2(x))
        x = self.max_pool(x)

        x = F.relu(self.conv2(x))
        x = F.relu(self.conv2(x))
        x = self.global_pool(x)

        x = x.view(-1, 64)

        x = F.relu(self.fc1(x))
        x = self.fc2(x)

        x = F.log_softmax(x)

        return x


model = Net()

n_epochs = 2
batch_size = 256

# download data
train_dataset = torchvision.datasets.MNIST(root='data', train=True,
                                           transform=transforms.ToTensor(), download=True)

test_dataset = torchvision.datasets.MNIST(root='data', train=False, transform=transforms.ToTensor())

# data loader
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size = batch_size,
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size = batch_size,
                                          shuffle=False)

# training
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
loss_f = nn.CrossEntropyLoss()

steps = len(train_loader)
for epoch in range(n_epochs):
    for i, (images, labels) in enumerate(train_loader):
        # forward
        output = model(images)
        loss = loss_f(output, labels)

        # backward
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if (i + 1) % 100 == 0:
            print('Epoch', epoch + 1, '/', n_epochs, 'Step', i + 1, '/', steps, 'Loss:', loss.item())

# testing
with torch.no_grad():
    correct = 0
    total = 0
    for images, labels in test_loader:
        output = model(images)
        _, predicted = torch.max(output.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

    print('Accuracy: ', correct / total)

torch.save(model.state_dict(), 'model.ckpt')


















